﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {
        private DataTable tblClientes;
        private DataSet dsClientes;
        private BindingSource bsClientes;
        private DataRow drCliente;

        public DataTable TblClientes { get => tblClientes; set => tblClientes = value; }
        public DataSet DsClientes { get => dsClientes; set => dsClientes = value; }

        public FrmCliente()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
        }

        public DataRow DrCliente
        {
            set
            {
                drCliente = value;
                msktCed.Text = drCliente["Cedula"].ToString();
                txtNombres.Text = drCliente["Nombre"].ToString();
                txtApellidos.Text = drCliente["Apellido"].ToString();
                msktTelefono.Text = drCliente["Telefono"].ToString();
                txtCorreo.Text = drCliente["Correo"].ToString();
                txtDireccion.Text = drCliente["Direccion"].ToString();
            }
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            string cedula, nombre, apellido, celular, correo, direccion;

            cedula = msktCed.Text;
            nombre = txtNombres.Text;
            apellido = txtApellidos.Text;
            celular = msktTelefono.Text;
            correo = txtCorreo.Text;
            direccion = txtDireccion.Text;

            if (drCliente != null)
            {
                DataRow drNew = TblClientes.NewRow();
                int index = TblClientes.Rows.IndexOf(drCliente);

                drNew["Id"] = drCliente["Id"];
                drNew["Cedula"] = cedula;
                drNew["Nombre"] = nombre;
                drNew["Apellido"] = apellido;
                drNew["Telefono"] = celular;
                drNew["Correo"] = correo;
                drNew["Direccion"] = direccion;

                TblClientes.Rows.RemoveAt(index);
                TblClientes.Rows.InsertAt(drNew, index);
            }
            else
            {
                TblClientes.Rows.Add(TblClientes.Rows.Count + 1, cedula, nombre, apellido, celular, correo, direccion);
            }

            Dispose();
        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bsClientes.DataSource = DsClientes;
            bsClientes.DataMember = DsClientes.Tables["Cliente"].TableName;
        }


        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}